package Hero;
import Equipments.Armor;
import Equipments.ArmorTypes;
import Equipments.Weapon;
import Equipments.WeaponTypes;
import Exceptions.InvalidArmourException;
import Exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemTests {
    //test items
    Rogue testRogue = new Rogue("Silent Eagle");
    Weapon testDagger = new Weapon("Dagger of Farmer",1,1.4, WeaponTypes.DAGGER,EquipmentSlots.WEAPON,4);
    Weapon testDaggerTooHighLevel = new Weapon("Dagger of Farmer",10,1.4, WeaponTypes.DAGGER,EquipmentSlots.WEAPON,4);
    Weapon testBow = new Weapon("Bow of Wanders", 1,2.3,WeaponTypes.BOW,EquipmentSlots.WEAPON,29);
    Armor testHeadMail = new Armor("Helmet of Peasant", 1, ArmorTypes.MAIL,EquipmentSlots.HEAD,3,1,2);
    Armor testHeadMailHighLevel = new Armor("Helmet of supreme Warrior", 11, ArmorTypes.MAIL,EquipmentSlots.HEAD,3,1,2);
    Armor testHeadPlate = new Armor("Helmet of weak plate", 1,ArmorTypes.PLATE,EquipmentSlots.HEAD,4,1,17);

    @Test
    //Test 1
    public void hero_WeaponToHighLevel_InvalidWeaponException(){  //Test to make sure hero can't equip an item that is too high level
        String expected = "Your level is not high enough to equip this weapon"; //Expected output from the exception
        String actual = null;
        try { //Try to put on an item that is too high level
            testRogue.equip(testDaggerTooHighLevel);
        } catch (InvalidWeaponException | InvalidArmourException e){  //Exception when the item is too high level
            actual = e.getMessage();
        }
        assertEquals(expected,actual);

    }
    @Test
    //Test 2
    public void hero_ArmorToHighLevel_InvalidArmorException(){ // same level test, but with armor
        String expected = "Your level is not high enough to equip this armour";
        String actual = null;
        try {
            testRogue.equip(testHeadMailHighLevel);
        } catch (InvalidWeaponException | InvalidArmourException e){
            actual = e.getMessage();
        }
        assertEquals(expected,actual);

    }
    @Test
    //Test 3
    public void hero_wrongWeaponType_ReturnInvalidWeaponException(){   //Test to make sure hero can't equip any else weapon than he is restricted to
        String expected = "Weapon type not supported by this class";
        String actual = null;
        try {
            testRogue.equip(testBow);
        } catch (InvalidWeaponException | InvalidArmourException e){
            actual = e.getMessage();
        }
        assertEquals(expected,actual);

    }
    @Test
    //Test 4
    public void hero_wrongArmorType_ReturnInvalidArmorException(){   // same restriction test, but with armor
        String expected = "Armor type not supported by this class";
        String actual = null;
        try {
            testRogue.equip(testHeadPlate);
        } catch (InvalidWeaponException | InvalidArmourException e){
            actual = e.getMessage();
        }
        assertEquals(expected,actual);

    }
    @Test
    //Test 5
    public void hero_validWeapon_ReturnTrue(){   // test to see if the weapon is equippable, if so, return true
        boolean expected = true;
        boolean actual = false;
        try {
            testRogue.equip(testDagger);
            actual = true; //return true if no exception was thrown
        } catch (InvalidWeaponException | InvalidArmourException e){
            actual = false; // return false if exception was thrown
        }
        assertEquals(expected,actual);

    }
    @Test
    //Test 6
    public void hero_validArmor_ReturnTrue(){ // same test, but with armor
        boolean expected = true;
        boolean actual = false;
        try {
            testRogue.equip(testHeadMail);
            actual = true;
        } catch (InvalidWeaponException | InvalidArmourException e){
            actual = false;
        }
        assertEquals(expected,actual);

    }
    @Test
    //Test 7
    void heroDps_is1_WhenCreated(){  //test to se if hero dps is 1 when created
        double expected = 1*(1+(5/100));
        double actual = testRogue.getDps();

        assertEquals(expected,actual);
    }

    @Test
    //Test 8
    void heroDps_isCorrect_WhenWeaponEquipped() throws InvalidWeaponException, InvalidArmourException { // test to see if hero dps increases when weapon is equipped
        double expected = 1.4*4*(1+(6/100.0));
        testRogue.equip(testDagger);  //equip dagger
        double actual = testRogue.getDps(); //return dps

        assertEquals(expected,actual);
    }
    @Test
    // Test 9
    void heroDps_isCorrect_WhenWeaponAndArmorEquipped() throws InvalidWeaponException, InvalidArmourException {
        double expected = 1.4*4*(1+((6+3)/100.0)); //Heroes start with starting gear, so the attributes
        testRogue.equip(testDagger);  // equip dagger
        testRogue.equip(testHeadMail);  // equip mail helmet
        double actual = testRogue.getDps();

        assertEquals(expected,actual);
    }
}
