package Hero;

import Equipments.Weapon;
import Equipments.WeaponTypes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    Weapon testSword = new Weapon("Sword of Destiny",3,1.4, WeaponTypes.SWORD,EquipmentSlots.WEAPON,24);
    Weapon testBow = new Weapon("bow Of Fate",1,2, WeaponTypes.BOW,EquipmentSlots.WEAPON,12);
    Mage testMage = new Mage("powerMan");
    Ranger testRanger = new Ranger("Miik");
    Warrior testWarrior = new Warrior("SatirPalparto");
    Rogue testRogue = new Rogue("EmperorQadriceps");


    @Test
    public void hero_shouldStartAt_level1(){
        int expected = 1;
        int actual = testMage.getLevel();

        assertEquals(expected,actual);
    }

    @Test
    public void hero_shouldLevelUp_toLevel2(){
        testMage.levelUp();
        int expected = 2;
        int actual = testMage.getLevel();

        assertEquals(expected,actual);
    }

    @Test
    public void mage_startsWithCorrect_attributes(){
        int expectedStr = 1;
        int expectedDex = 1;
        int expectedInt = 8;

        int actualStr = testMage.getBasePrimAtrStr();
        int actualDex = testMage.getBasePrimAtrDex();
        int actualInt = testMage.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }
    @Test
    public void rogue_startsWithCorrect_attributes(){
        int expectedStr = 2;
        int expectedDex = 6;
        int expectedInt = 1;

        int actualStr = testRogue.getBasePrimAtrStr();
        int actualDex = testRogue.getBasePrimAtrDex();
        int actualInt = testRogue.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }
    @Test
    public void warrior_startsWithCorrect_attributes(){
        int expectedStr = 5;
        int expectedDex = 2;
        int expectedInt = 1;

        int actualStr = testWarrior.getBasePrimAtrStr();
        int actualDex = testWarrior.getBasePrimAtrDex();
        int actualInt = testWarrior.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }
    @Test
    public void ranger_startsWithCorrect_attributes(){
        int expectedStr = 1;
        int expectedDex = 7;
        int expectedInt = 1;

        int actualStr = testRanger.getBasePrimAtrStr();
        int actualDex = testRanger.getBasePrimAtrDex();
        int actualInt = testRanger.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }
    @Test
    public void mage_GainsCorrect_attributesWhenLevelUp(){
        testMage.levelUp();

        int expectedStr = 2;
        int expectedDex = 2;
        int expectedInt = 13;

        int actualStr = testMage.getBasePrimAtrStr();
        int actualDex = testMage.getBasePrimAtrDex();
        int actualInt = testMage.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }
    @Test
    public void rogue_GainsCorrect_attributesWhenLevelUp(){
        testRogue.levelUp();

        int expectedStr = 3;
        int expectedDex = 10;
        int expectedInt = 2;

        int actualStr = testRogue.getBasePrimAtrStr();
        int actualDex = testRogue.getBasePrimAtrDex();
        int actualInt = testRogue.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }
    @Test
    public void warrior_GainsCorrect_attributesWhenLevelUp(){
        testWarrior.levelUp();

        int expectedStr = 8;
        int expectedDex = 4;
        int expectedInt = 2;

        int actualStr = testWarrior.getBasePrimAtrStr();
        int actualDex = testWarrior.getBasePrimAtrDex();
        int actualInt = testWarrior.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }
    @Test
    public void ranger_GainsCorrect_attributesWhenLevelUp(){
        testRanger.levelUp();

        int expectedStr = 2;
        int expectedDex = 12;
        int expectedInt = 2;

        int actualStr = testRanger.getBasePrimAtrStr();
        int actualDex = testRanger.getBasePrimAtrDex();
        int actualInt = testRanger.getBasePrimAtrInt();

        assertEquals(expectedStr,actualStr);
        assertEquals(expectedDex,actualDex);
        assertEquals(expectedInt,actualInt);

    }



}