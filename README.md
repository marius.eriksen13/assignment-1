!---development has stopped completely---!
#Assignment 1: Rpg console application

## Description
This is a school project i did through Noroff in Bergen.
The project is an application where you can create heroes, level them up, give them items and check their attributes and damage.
You can create new items(armor and gear) that you can put on your hero. 

When you create a hero there is 4 calsses you can chose from, warrior, mage, rogue and ranger.

## Attributes
each hero starts with their own set of attributes:
Mage - Strength: 1, Dexterity 1, Intelligence 8
Warrior - Strength: 5, Dexterity: 2, Intelligence: 1
Rogue - Strength: 2, Dexterity: 6, Intelligence: 1
Ranger - Strength: 1, Dexterity: 7, Intelligence: 1

Depending on which hero you chose you have a primary attribute
Mage - Intelligence
Warrior - Strength
Ranger - Dexterity
Rogue - Dexterity

## Restrictions
Each hero has their set of restrictions on gear
Mage - cloth, wand and staff
Warrior - plate, mail, axe, hammer and sword
Rogue - mail, leather, sword and dagger
Ranger - leather and bow

## Installation
To try this application out for your selfe you just need to clone the repo with ssh and run it in the terminal.

## Tests
To run the tests, you need to right click the green test folder and click "Run 'All tests'"

## Author and acknowledgement
Code written by: Marius Eriksen
Lecturer: Livinus Obiora Nweke, Lecturer at Noroff University College

## License
This code is open source and for anyone to use. Has to be credited
