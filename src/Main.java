import Equipments.Armor;
import Equipments.ArmorTypes;
import Equipments.Weapon;
import Equipments.WeaponTypes;
import Exceptions.InvalidArmourException;
import Exceptions.InvalidWeaponException;
import Hero.*;

public class Main {
    public static void main(String[] args) throws InvalidWeaponException, InvalidArmourException {

        Rogue rogue = new Rogue("Rogu");
        Warrior warrior = new Warrior("Warrix");
        Mage mage = new Mage("Magus");
        Ranger ranger = new Ranger("Randulf");

        Weapon hammer = new Weapon("Hammer of thor", 5, 1, WeaponTypes.HAMMER, EquipmentSlots.WEAPON, 78);
        Weapon wand = new Weapon("Wand of Wanders", 2, 2, WeaponTypes.WAND, EquipmentSlots.WEAPON, 25);
        Weapon bow = new Weapon("Eagle eye", 1, 0.5, WeaponTypes.BOW, EquipmentSlots.WEAPON, 200);
        Weapon dagger = new Weapon("Dagger of Eternity", 4, 4, WeaponTypes.DAGGER, EquipmentSlots.WEAPON, 15);

        Armor plateHeadgear = new Armor("Helmet of Power", 3,ArmorTypes.PLATE,EquipmentSlots.HEAD,4,1,17);
        Armor clothHeadgear = new Armor("Helmet of Speed", 2,ArmorTypes.CLOTH,EquipmentSlots.HEAD,5,10,1);
        Armor mailHeadGear = new Armor("Helmet of Growth", 1,ArmorTypes.MAIL,EquipmentSlots.HEAD,6,1,8);
        Armor leatherHeadGear = new Armor("Helmet of Relentlessness", 1,ArmorTypes.LEATHER,EquipmentSlots.HEAD,9,1,4);

        Armor plateChestgear = new Armor("Chest of the Dragon", 1,ArmorTypes.PLATE,EquipmentSlots.CHEST,2,1,17);
        Armor clothChestgear = new Armor("Cloth Chest of fairy Dust", 1,ArmorTypes.CLOTH,EquipmentSlots.CHEST,3,8,2);
        Armor mailChestgear = new Armor("Chest of the Goblin God Smith", 1,ArmorTypes.MAIL,EquipmentSlots.CHEST,10,1,5);
        Armor leatherChestgear = new Armor("Brynjulfs Chest", 1,ArmorTypes.LEATHER,EquipmentSlots.CHEST,11,2,1);

        Armor plateLegs = new Armor("Legs of Legs", 2,ArmorTypes.PLATE,EquipmentSlots.LEGS,1,1,8);
        Armor clothLegs = new Armor("Leandrys legs", 2,ArmorTypes.CLOTH,EquipmentSlots.LEGS,4,10,9);
        Armor mailLegs = new Armor("Legs of Silent Bull", 2,ArmorTypes.MAIL,EquipmentSlots.LEGS,21,4,3);
        Armor leatherLegs = new Armor("Legs of Lion", 2,ArmorTypes.LEATHER,EquipmentSlots.LEGS,2,4,5);

        System.out.println(warrior.getStatDisplay());
        System.out.println(mage.getStatDisplay());
        System.out.println(ranger.getStatDisplay());
        System.out.println(rogue.getStatDisplay());

        warrior.levelUp();
        warrior.levelUp();
        warrior.levelUp();
        warrior.levelUp();
        warrior.levelUp();

        mage.levelUp();
        mage.levelUp();

        rogue.levelUp();
        rogue.levelUp();
        rogue.levelUp();

        warrior.equip(hammer);
        mage.equip(wand);
        ranger.equip(bow);
        rogue.equip(dagger);

        System.out.println(warrior.getStatDisplay());
        System.out.println(mage.getStatDisplay());
        System.out.println(ranger.getStatDisplay());
        System.out.println(rogue.getStatDisplay());

        System.out.println();
        System.out.println(warrior.dealDamage());
        System.out.println();
        System.out.println(mage.dealDamage());
        System.out.println();
        System.out.println(ranger.dealDamage());
        System.out.println();
        System.out.println(rogue.dealDamage());

        warrior.equip(plateHeadgear);
        mage.equip(clothHeadgear);
        ranger.equip(mailHeadGear);
        rogue.equip(leatherHeadGear);

        warrior.equip(plateChestgear);
        mage.equip(clothChestgear);
        ranger.equip(mailChestgear);
        rogue.equip(leatherChestgear);

        ranger.levelUp();

        warrior.equip(plateLegs);
        mage.equip(clothLegs);
        ranger.equip(mailLegs);
        rogue.equip(leatherLegs);
        warrior.equip(plateLegs);

    }
}
