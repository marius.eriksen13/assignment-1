package Hero;

import Equipments.*;
import Exceptions.InvalidArmourException;
import Exceptions.InvalidWeaponException;
import Interfaces.IHero;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Hero implements IHero {

    // create some variables that is nice to have
    private String name;
    private int level = 1;
    private double dps = 1;
    private int basePrimAtrStr;
    private int basePrimAtrInt;
    private int basePrimAtrDex;
    private int totalPrimAtrStr;
    private int totalPrimAtrInt;
    private int totalPrimAtrDex;
    protected ArrayList<WeaponTypes> weaponRestrictions = new ArrayList<WeaponTypes>();
    protected ArrayList<ArmorTypes> armorRestrictions = new ArrayList<ArmorTypes>();
    protected static HashMap<EquipmentSlots, Item> equipmentSlot = new HashMap<EquipmentSlots,Item>(); // hashmap for storing items

    public Hero(String name){ // take inn name
        this.name = name;

        this.equipmentSlot.put(EquipmentSlots.WEAPON,null); //add equipment slots to HashMap
        this.equipmentSlot.put(EquipmentSlots.HEAD, null);
        this.equipmentSlot.put(EquipmentSlots.CHEST, null);
        this.equipmentSlot.put(EquipmentSlots.LEGS, null);

    }

    public void equip(Item item) throws InvalidWeaponException, InvalidArmourException { //ethod for equiping items
        System.out.println();
        if (item.getLevelRequirement() <= getLevel()) { // check if hero is high enough level
            if (item.getSlotType().equals(EquipmentSlots.WEAPON)) { //check if the item is a weapon
                Weapon weapon = (Weapon) item; // if it is a weapon, cast it to weapon
                if (this.weaponRestrictions.contains(weapon.getWeaponType())) { //check if the weapon is in the restricted to list
                    this.equipmentSlot.replace(EquipmentSlots.WEAPON, weapon); // replace weapon from equipmentSlot
                    System.out.println(weapon.getName()+" has been equipped");
                    calculateDps(); //since the weapon has been changer, ou have to calculate new dps
                } else {
                    throw new InvalidWeaponException("Weapon type not supported by this class");
                }

            } else if (item.getSlotType().equals(EquipmentSlots.WEAPON) == false) { // if it's not a weapon, it's an armor piece
                Armor armor = (Armor) item; //cast to armor
                EquipmentSlots equipWhere = armor.getSlotType();

                if (armorRestrictions.contains(armor.getArmorType())) { // check if hero can use the piece
                    this.setTotalPrimAtrInt(getTotalPrimAtrInt() - this.equipmentSlot.get(equipWhere).getAtrInt()); //subtract the attributes from the old gear piece
                    this.setTotalPrimAtrDex(getTotalPrimAtrDex() - this.equipmentSlot.get(equipWhere).getAtrDex());
                    this.setTotalPrimAtrStr(getTotalPrimAtrStr() - this.equipmentSlot.get(equipWhere).getAtrStr());

                    this.equipmentSlot.replace(equipWhere, armor);

                    this.setTotalPrimAtrInt(getTotalPrimAtrInt() + this.equipmentSlot.get(equipWhere).getAtrInt()); //add attributes from new gear piece
                    this.setTotalPrimAtrDex(getTotalPrimAtrDex() + this.equipmentSlot.get(equipWhere).getAtrDex());
                    this.setTotalPrimAtrStr(getTotalPrimAtrStr() + this.equipmentSlot.get(equipWhere).getAtrStr());

                    calculateDps();
                    System.out.println(armor.getName()+" has been equipped");
                } else {
                    throw new InvalidArmourException("Armor type not supported by this class"); // throw invalidArmorException if the armor can't be used by the hero
                }
            }
        } else {
            if (item.getSlotType().equals((EquipmentSlots.WEAPON)) == true) { //check what piece it was, so you can know what exception to throw if level is too high
                throw new InvalidWeaponException("Your level is not high enough to equip this weapon");
            } else {
                throw new InvalidArmourException("Your level is not high enough to equip this armour");
            }
        }
    }
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getDps() {
        return dps;
    }

    public void setDps(double dps) {
        this.dps = dps;
    }

    public String getName() {
        return name;
    }

    public void setHeroClass(String heroClass) {
        heroClass = heroClass;
    }

    public int getBasePrimAtrStr() {
        return basePrimAtrStr;
    }

    public void setBasePrimAtrStr(int basePrimAtrStr) {
        this.basePrimAtrStr = basePrimAtrStr;
    }

    public int getBasePrimAtrInt() {
        return basePrimAtrInt;
    }

    public void setBasePrimAtrInt(int basePrimAtrInt) {
        this.basePrimAtrInt = basePrimAtrInt;
    }

    public int getBasePrimAtrDex() {
        return basePrimAtrDex;
    }

    public void setBasePrimAtrDex(int basePrimAtrDex) {
        this.basePrimAtrDex = basePrimAtrDex;
    }

    public int getTotalPrimAtrStr() {
        return totalPrimAtrStr;
    }

    public void setTotalPrimAtrStr(int totalPrimAtrStr) {
        this.totalPrimAtrStr = totalPrimAtrStr;
    }

    public int getTotalPrimAtrInt() {
        return totalPrimAtrInt;
    }

    public void setTotalPrimAtrInt(int totalPrimAtrInt) {
        this.totalPrimAtrInt = totalPrimAtrInt;
    }

    public int getTotalPrimAtrDex() {
        return totalPrimAtrDex;
    }

    public void setTotalPrimAtrDex(int totalPrimAtrDex) {
        this.totalPrimAtrDex = totalPrimAtrDex;
    }
    public String getEquipment() {
        return "Weapon: "+ equipmentSlot.get(EquipmentSlots.WEAPON).getName()+" || Head: "+ equipmentSlot.get(EquipmentSlots.HEAD).getName()+" || Chest: "+ equipmentSlot.get(EquipmentSlots.CHEST).getName()+" || Legs: " + equipmentSlot.get(EquipmentSlots.LEGS).getName();
    }

    public double damagetoEnemy(){
        double dmgVariable = Math.random()*(1-0+1);//variable for that makes dmg wary from attack to attack
        if (dmgVariable >1.2){ // if damage is *1.2 or higher, it's a critical hit
            System.out.println("That's a critical hit!");
        } else if (dmgVariable <0.8){ //if dmg is *o.8 or lower, it's a weak ht
            System.out.println("Ohh no, that's a weak hit!");
        }
        return this.calculateDps()*dmgVariable;
    }

    public String dealDamage(){
        return this.getName()+" did "+damagetoEnemy()+" damage to the enemy";
    }
    public String getStatDisplay(){
        return "Name: "+ getName() +" || Level: "+ getLevel() +" || Strength: "+getTotalPrimAtrStr()+" || Dexterity: "+getTotalPrimAtrDex()+" || Intelligence: "+ getTotalPrimAtrInt()+" || DPS: "+ getDps();
    }
    public static HashMap<EquipmentSlots, Item> getEquipmentSlot() {
        return equipmentSlot;
    }
}
