package Hero;

import Equipments.*;
// ------------------------!!!!!LOOK AT ROGUE FOR COMMENTS!!!!!----------------------

public class Mage extends Hero {
    public Mage(String name) {
        super(name);
        super.setBasePrimAtrStr(1);
        super.setBasePrimAtrDex(1);
        super.setBasePrimAtrInt(8);
        super.setHeroClass("MAGE");
        super.setDps(1*(1+(getBasePrimAtrInt()/100)));


        Weapon startingStaff = new Weapon("Starting staff",0,1.5, WeaponTypes.STAFF, EquipmentSlots.WEAPON,100);

        Armor startingHelmet = new Armor("Starting helmet",0, ArmorTypes.CLOTH,EquipmentSlots.HEAD,0,0,0);
        Armor startingChest = new Armor("Starting chest",0, ArmorTypes.CLOTH,EquipmentSlots.CHEST,0,0,0);
        Armor startingLegs = new Armor("Starting legs",0, ArmorTypes.CLOTH,EquipmentSlots.LEGS,0,0,0);

        equipmentSlot.replace(EquipmentSlots.WEAPON,startingStaff);
        equipmentSlot.replace(EquipmentSlots.HEAD,startingHelmet);
        equipmentSlot.replace(EquipmentSlots.CHEST,startingChest);
        equipmentSlot.replace(EquipmentSlots.LEGS,startingLegs);

        setTotalPrimAtrInt(getBasePrimAtrInt()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrInt()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrInt()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrInt());
        setTotalPrimAtrStr(getBasePrimAtrStr()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrStr()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrStr()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrStr());
        setTotalPrimAtrDex(getBasePrimAtrDex()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrDex()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrDex()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrDex());

        armorRestrictions.add(ArmorTypes.CLOTH);
        weaponRestrictions.add(WeaponTypes.STAFF);
        weaponRestrictions.add(WeaponTypes.WAND);
    }

    @Override
    public void levelUp() {

        int level = getLevel();
        int strAtr = getBasePrimAtrStr()+ getTotalPrimAtrStr();
        int dexAtr = getBasePrimAtrDex()+ getTotalPrimAtrDex();
        int intAtr = getBasePrimAtrInt()+ getTotalPrimAtrInt();

        setLevel(level + 1);

        setBasePrimAtrStr(getBasePrimAtrStr() + 1);
        setBasePrimAtrDex(getBasePrimAtrDex() + 1);
        setBasePrimAtrInt(getBasePrimAtrInt() + 5);

        setTotalPrimAtrStr(strAtr + 1);
        setTotalPrimAtrDex(dexAtr + 1);
        setTotalPrimAtrInt(intAtr + 5);
    }

    public double calculateDps(){
        Weapon weapon = (Weapon) equipmentSlot.get(EquipmentSlots.WEAPON);
        double dps = weapon.getDps()*(1+Double.valueOf(this.getTotalPrimAtrInt())/100);
        this.setDps((dps));
        return getDps();
    }

}
