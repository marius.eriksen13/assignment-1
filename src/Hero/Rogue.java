package Hero;

import Equipments.*;
public class Rogue extends Hero {
    public Rogue(String name) {
        super(name);
        super.setBasePrimAtrStr(2);
        super.setBasePrimAtrDex(6);
        super.setBasePrimAtrInt(1);
        super.setHeroClass("ROGUE");
        super.setDps(1*(1+(getBasePrimAtrDex()/100)));

        //created starting gear, since no hero should be naked
        Weapon startingDagger = new Weapon("Starting Dagger",0,1.5, WeaponTypes.DAGGER, EquipmentSlots.WEAPON, 25);

        Armor startingHelmet = new Armor("Starting helmet",0, ArmorTypes.CLOTH,EquipmentSlots.HEAD,0,0,0);
        Armor startingChest = new Armor("Starting chest",0, ArmorTypes.CLOTH,EquipmentSlots.CHEST,0,0,0);
        Armor startingLegs = new Armor("Starting legs",0, ArmorTypes.CLOTH,EquipmentSlots.LEGS,0,0,0);

        //equip the items
        equipmentSlot.replace(EquipmentSlots.WEAPON,startingDagger);
        equipmentSlot.replace(EquipmentSlots.HEAD,startingHelmet);
        equipmentSlot.replace(EquipmentSlots.CHEST,startingChest);
        equipmentSlot.replace(EquipmentSlots.LEGS,startingLegs);

        //set total atributes to have gear attributes
        setTotalPrimAtrInt(getBasePrimAtrInt()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrInt()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrInt()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrInt());
        setTotalPrimAtrStr(getBasePrimAtrStr()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrStr()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrStr()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrStr());
        setTotalPrimAtrDex(getBasePrimAtrDex()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrDex()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrDex()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrDex());

        // add armor and weapons to restriction lists. hero can only use these
        armorRestrictions.add(ArmorTypes.MAIL);
        armorRestrictions.add(ArmorTypes.LEATHER);
        weaponRestrictions.add(WeaponTypes.DAGGER);
        weaponRestrictions.add(WeaponTypes.SWORD);
    }

    @Override
    public void levelUp() {
        int strAtr = getBasePrimAtrStr() + getTotalPrimAtrStr();
        int dexAtr = getBasePrimAtrDex() + getTotalPrimAtrDex();
        int intAtr = getBasePrimAtrInt() + getTotalPrimAtrInt();

        int level = getLevel();
        setLevel(level + 1); //get 1 level when level up.

        //attributes that are added every time levelUp() is called
        setBasePrimAtrStr(getBasePrimAtrStr() + 1);
        setBasePrimAtrDex(getBasePrimAtrDex() + 4);
        setBasePrimAtrInt(getBasePrimAtrInt() + 1);
        //ad attributes to total attributes
        setTotalPrimAtrStr(strAtr + 1);
        setTotalPrimAtrDex(dexAtr + 4);
        setTotalPrimAtrInt(intAtr + 1);
    }

    public double calculateDps(){ //method for calcuating dps
        Weapon weapon = (Weapon) equipmentSlot.get(EquipmentSlots.WEAPON);// get EquipmentSlots.WEAPON and cast it as weapon
        double dps = weapon.getDps()*(1+Double.valueOf(this.getTotalPrimAtrDex())/100); //weapon dmg*attackspeed * attribute scaling
        this.setDps((dps));
        return getDps();
    }

}
