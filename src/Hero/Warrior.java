package Hero;

import Equipments.*;
// ------------------------!!!!!LOOK AT ROGUE FOR COMMENTS!!!!!----------------------

public class Warrior extends Hero {
    public Warrior(String name) {
        super(name);
        super.setBasePrimAtrStr(5);
        super.setBasePrimAtrDex(2);
        super.setBasePrimAtrInt(1);
        super.setHeroClass("WARRIOR");
        super.setDps(1*(1+(getBasePrimAtrStr()/100)));

        Weapon startingAxe = new Weapon("Starting axe",0,0.5, WeaponTypes.AXE, EquipmentSlots.WEAPON,150);

        Armor startingHelmet = new Armor("Starting helmet",0, ArmorTypes.PLATE,EquipmentSlots.HEAD,0,0,0);
        Armor startingChest = new Armor("Starting chest",0, ArmorTypes.MAIL,EquipmentSlots.CHEST,0,0,0);
        Armor startingLegs = new Armor("Starting legs",0, ArmorTypes.PLATE,EquipmentSlots.LEGS,0,0,0);

        equipmentSlot.replace(EquipmentSlots.WEAPON,startingAxe);
        equipmentSlot.replace(EquipmentSlots.HEAD,startingHelmet);
        equipmentSlot.replace(EquipmentSlots.CHEST,startingChest);
        equipmentSlot.replace(EquipmentSlots.LEGS,startingLegs);

        setTotalPrimAtrInt(getBasePrimAtrInt()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrInt()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrInt()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrInt());
        setTotalPrimAtrStr(getBasePrimAtrStr()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrStr()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrStr()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrStr());
        setTotalPrimAtrDex(getBasePrimAtrDex()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrDex()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrDex()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrDex());

        armorRestrictions.add(ArmorTypes.PLATE);
        armorRestrictions.add(ArmorTypes.MAIL);
        weaponRestrictions.add(WeaponTypes.AXE);
        weaponRestrictions.add(WeaponTypes.HAMMER);
        weaponRestrictions.add(WeaponTypes.SWORD);

    }

    @Override
    public void levelUp() {
        int level = getLevel();
        int strAtr = getBasePrimAtrStr() + getTotalPrimAtrStr();
        int dexAtr = getBasePrimAtrDex() + getTotalPrimAtrDex();
        int intAtr = getBasePrimAtrInt() + getTotalPrimAtrInt();

        setLevel(level + 1);

        setBasePrimAtrStr(getBasePrimAtrStr() + 3);
        setBasePrimAtrDex(getBasePrimAtrDex() + 2);
        setBasePrimAtrInt(getBasePrimAtrInt() + 1);

        setTotalPrimAtrStr(strAtr + 3);
        setTotalPrimAtrDex(dexAtr + 2);
        setTotalPrimAtrInt(intAtr + 1);
    }

    public double calculateDps(){
        Weapon weapon = (Weapon) equipmentSlot.get(EquipmentSlots.WEAPON);
        double dps = weapon.getDps()*(1+Double.valueOf(this.getTotalPrimAtrStr())/100);
        this.setDps((dps));
        return getDps();
    }

}
