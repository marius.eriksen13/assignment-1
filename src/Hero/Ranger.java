package Hero;

import Equipments.*;
// ------------------------!!!!!LOOK AT ROGUE FOR COMMENTS!!!!!----------------------

public class Ranger extends Hero {
    public Ranger(String name) {
        super(name);
        super.setBasePrimAtrStr(1);
        super.setBasePrimAtrDex(7);
        super.setBasePrimAtrInt(1);
        super.setHeroClass("RANGER");
        super.setDps(1*(1+(getBasePrimAtrDex()/100)));


        Weapon startingBow = new Weapon("Starting bow",0,1.5, WeaponTypes.BOW, EquipmentSlots.WEAPON,50);

        Armor startingHelmet = new Armor("Starting helmet",0, ArmorTypes.LEATHER,EquipmentSlots.HEAD,0,0,0);
        Armor startingChest = new Armor("Starting chest",0, ArmorTypes.MAIL,EquipmentSlots.CHEST,0,0,0);
        Armor startingLegs = new Armor("Starting legs",0, ArmorTypes.LEATHER,EquipmentSlots.LEGS,0,0,0);

        equipmentSlot.replace(EquipmentSlots.WEAPON,startingBow);
        equipmentSlot.replace(EquipmentSlots.HEAD,startingHelmet);
        equipmentSlot.replace(EquipmentSlots.CHEST,startingChest);
        equipmentSlot.replace(EquipmentSlots.LEGS,startingLegs);

        setTotalPrimAtrInt(getBasePrimAtrInt()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrInt()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrInt()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrInt());
        setTotalPrimAtrStr(getBasePrimAtrStr()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrStr()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrStr()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrStr());
        setTotalPrimAtrDex(getBasePrimAtrDex()+ equipmentSlot.get(EquipmentSlots.HEAD).getAtrDex()+ equipmentSlot.get(EquipmentSlots.CHEST).getAtrDex()+ equipmentSlot.get(EquipmentSlots.LEGS).getAtrDex());

        armorRestrictions.add(ArmorTypes.LEATHER);
        armorRestrictions.add(ArmorTypes.MAIL);
        weaponRestrictions.add(WeaponTypes.BOW);

    }

    @Override
    public void levelUp() {
        int level = getLevel();

        int strAtr = getBasePrimAtrStr()+ getTotalPrimAtrStr();
        int dexAtr = getBasePrimAtrDex()+ getTotalPrimAtrDex();
        int intAtr = getBasePrimAtrInt()+ getTotalPrimAtrInt();

        setLevel(level + 1);

        setBasePrimAtrStr(getBasePrimAtrStr() + 1);
        setBasePrimAtrDex(getBasePrimAtrDex() + 5);
        setBasePrimAtrInt(getBasePrimAtrInt() + 1);

        setTotalPrimAtrStr(strAtr + 1);
        setTotalPrimAtrDex(dexAtr + 5);
        setTotalPrimAtrInt(intAtr + 1);
    }

    public double calculateDps(){
        Weapon weapon = (Weapon) equipmentSlot.get(EquipmentSlots.WEAPON);
        double dps = weapon.getDps()*(1+Double.valueOf(this.getTotalPrimAtrDex())/100);
        this.setDps((dps));
        return getDps();
    }

}
