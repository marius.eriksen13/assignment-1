package Hero;

public enum EquipmentSlots {
    HEAD,
    CHEST,
    LEGS,
    WEAPON
}
