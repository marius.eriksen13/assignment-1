package Exceptions;

public class InvalidWeaponException extends Exception {

    public InvalidWeaponException(String msg) {
        super(msg);
    }
}
