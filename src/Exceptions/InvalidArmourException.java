package Exceptions;

public class InvalidArmourException extends Exception {

    public InvalidArmourException(String msg) {
        super(msg);
    }
}
