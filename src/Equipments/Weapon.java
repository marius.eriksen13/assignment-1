package Equipments;

import Hero.EquipmentSlots;


public class Weapon extends Item{
    private final EquipmentSlots SlotType = EquipmentSlots.WEAPON;
    private double attackSpeed;
    private WeaponTypes weaponType;
    private EquipmentSlots eslot;
    private double weaponDmg;
    private double dps;

    public Weapon(String name, int LevelRequirement, double attackSpeed, WeaponTypes weaponType, EquipmentSlots eslot, double weaponDmg) { // these are needed for every weapon
        //variables that's extra for weapon
        super(name, LevelRequirement, eslot);
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
        this.eslot = eslot;
        this.weaponDmg = weaponDmg;
        this.dps = weaponDmg*attackSpeed;


    }
    public EquipmentSlots getSlotType() {
        return SlotType;
    }

    public WeaponTypes getWeaponType() {
        return weaponType;
    }

    public double getDps() {
        return dps;
    }
}
