package Equipments;


import Hero.EquipmentSlots;


public abstract class Item {
    private String name;
    private int atrStr;
    private int atrDex;
    private int atrInt;
    private int levelRequirement;
    private EquipmentSlots slotType;
    public Item(String name, int levelRequirement, EquipmentSlots slotType) { //these are needed for every item
        this.name = name;
        this.levelRequirement = levelRequirement;
        this.slotType = slotType;
    }
    public int getAtrStr() {
        return atrStr;
    }

    public void setAtrStr(int atrStr) {
        this.atrStr = atrStr;
    }

    public int getAtrDex() {
        return atrDex;
    }

    public void setAtrDex(int atrDex) {
        this.atrDex = atrDex;
    }

    public int getAtrInt() {
        return atrInt;
    }

    public void setAtrInt(int atrInt) {
        this.atrInt = atrInt;
    }

    public int getLevelRequirement() {
        return levelRequirement;
    }

    public String getName() {
        return name;
    }

    public EquipmentSlots getSlotType() {
        return slotType;
    }

}
