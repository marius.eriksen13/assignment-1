package Equipments;

import Hero.EquipmentSlots;

public class Armor extends Item {
    private ArmorTypes armorType;
    private EquipmentSlots armorSlot;

    public Armor(String name, int LevelRequirement, ArmorTypes aType, EquipmentSlots eSlot, int atrDex, int atrInt, int atrStr) {
        super(name, LevelRequirement, eSlot);

        this.armorType = aType;
        this.armorSlot = eSlot;

        setAtrDex(atrDex);
        setAtrInt(atrInt);
        setAtrStr(atrStr);
    }

    public ArmorTypes getArmorType() {
        return armorType;
    }
}
