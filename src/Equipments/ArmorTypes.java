package Equipments;

public enum ArmorTypes {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE,
}
